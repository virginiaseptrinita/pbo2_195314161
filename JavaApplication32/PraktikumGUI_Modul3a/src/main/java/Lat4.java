/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author WIN10
 */
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Lat4 extends JFrame {

    public Lat4() {
        this.setLayout(null);
        this.setSize(300, 150);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);

        JLabel label = new JLabel("Keyword : ");
        label.setBounds(120, 10, 100, 20);
        this.add(label);
        JTextField field = new JTextField();
        field.setBounds(10, 30, 270, 20);
        this.add(field);
        JButton button = new JButton("Find");
        button.setBounds(110, 60, 70, 20);
        this.add(button);
    }

    public static void main(String[] args) {
        new Lat4();

    }

}
