/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.praktikumgui_modul3a;

/**
 *
 * @author WIN10
 */
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Lat3 extends JFrame {

    public Lat3() {
        this.setSize(300, 500);
//        setLocation(300,100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini class Turunan dari class JFrame");
        this.setVisible(true);
        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("Ini Tombol");
        panel.add(tombol);
        this.add(panel);

        JLabel label = new JLabel("JLabel");
        label.setBounds(10, 30, 200, 20);
        this.add(label);
        JTextField field = new JTextField("JtextField");
        field.setBounds(30, 60, 200, 20);
        this.add(field);
        JRadioButton button = new JRadioButton("Button");
        button.setBounds(220, 40, 100, 20);
        this.add(button);

        JCheckBox box = new JCheckBox("Box");
        box.setBounds(120, 70, 100, 20);
        this.add(box);

    }

    public static void main(String[] args) {
        new Lat3();

    }

}
